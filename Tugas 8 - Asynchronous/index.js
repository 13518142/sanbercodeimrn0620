// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000

// Tulis code untuk memanggil function readBooks di sini
function calculate(idx){
    if(idx >= books.length){
        return 0
    } else {
        readBooks(time,books[idx],function(){
            time -= books[idx].timeSpent
            calculate(idx+1)
        })
    }
}

calculate(0)
