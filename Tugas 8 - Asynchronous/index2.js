var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

let time = 10000
// Lanjutkan code untuk menjalankan function readBooksPromise 
function calcPromise(idx){
    if (idx >= books.length){
        return 0
    } else{
        readBooksPromise(time,(books[idx]))
        .then(function(finished){
            time -= books[idx].timeSpent
            calcPromise(idx+1)
        })
        .catch(function(){
        })

    }
}
// console.log(books[0].name)
calcPromise(0)