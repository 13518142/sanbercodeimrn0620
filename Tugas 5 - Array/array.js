//Oleh: Dwiani Yulia Ariyanti

//////////////
/// Soal 1 ///
//////////////


function range(startNum, finishNum) {
    var num = []
    if (startNum == null || finishNum == null){
        return -1
    } else {
        if (finishNum > startNum){
            while (startNum <= finishNum){
                num.push(startNum)
                startNum ++
            }
        } else {
            while (finishNum <= startNum){
                num.push(startNum)
                startNum --
            }
        }
        return num
    }
}


//////////////
/// Soal 2 ///
//////////////

function rangeWithStep(startNum, finishNum, step) {
    var num = []
    if (startNum == null || finishNum == null){
        return -1
    } else {
        if (finishNum > startNum){
            while (startNum <= finishNum){
                num.push(startNum)
                startNum += step
            }
        } else {
            while (finishNum <= startNum){
                num.push(startNum)
                startNum -= step
            }
        }
        return num
    }
}


//////////////
/// Soal 3 ///
//////////////

function sum(startNum, finishNum, step){
    if (startNum == null || finishNum == null){
        if (startNum == null && finishNum == null){
            return 0
        } else {
            return startNum
        }
    } else {
        var array = []
        if (step == null){
            array = range(startNum,finishNum)
        } else {
            array = rangeWithStep(startNum,finishNum,step)
        }
        var sum_val = 0
        while (array.length > 0){
            sum_val += array.pop()
        }
        return sum_val;
    }
}

//////////////
/// Soal 4 ///
//////////////

function dataHandling(array){
    var str = ''
    var arr = []
    for (var i = 0; i < array.length; i++){
        data = array[i]
        str += ("Nomor ID: " + data[0] + "\n")
        str += ("Nama Lengkap: " + data[1] + "\n")
        str += ("TTL: " + data[2] + " " + data[3] + "\n")
        str += ("Hobi: " + data[4] + "\n\n")
    }
    return (str)
}

//////////////
/// Soal 5 ///
//////////////

function balikKata(str){
    rev = ''
    for (i = str.length -1; i >=0 ; i--){
        rev += (str[i])
    }
    return rev
}


//////////////
/// Soal 6 ///
//////////////

function dataHandling2(input){

    input.splice(1,1,input[1] + " Elsharawy")
    input.splice(2,1,"Provinsi " + input[2])
    input.splice(4,0,"Pria")
    input.splice(5,1,"SMA Internasional Metro")
    console.log(input)


    date = input[3].split("/")
    switch(parseInt(date[1])){
        case 01 : {console.log( 'Januari' );break;}
        case 02 : {console.log( 'Februari' );break;}
        case 03 : {console.log( 'Maret' );break;}
        case 04 : {console.log( 'April' );break;}
        case 05 : {console.log( 'Mei' );break;}
        case 06 : {console.log( 'Juni' );break;}
        case 07 : {console.log( 'Juli' );break;}
        case 08 : {console.log( 'Agustus' );break;}
        case 09 : {console.log( 'September' );break;}
        case 10 : {console.log( 'Oktober' );break;}
        case 11 : {console.log( 'November' );break;}
        case 12 : {console.log( 'Desember' );break;}
    }
    date_join = date.join("-")
    date_sort = (date.sort(function(a, b){return b-a}))
    console.log(date_sort)
    console.log(date_join)
    console.log(input[1].slice(0,14))
}






/////////////
/// CHECK ///
/////////////


//Soal 1 Check

console.log("Soal No. 1 (Range)")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54,50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()

//Soal 2 Check

console.log("Soal No. 2 (Range with Step)")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log()

//Soal 3 Check

console.log("Soal No. 3 (Sum of Range)")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log()

//Soal 4 Check

console.log("Soal No. 4 (Array Multidimensi)")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input))
console.log()

//Soal 5 Check

console.log("Soal No. 5 (Balik Kata)")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log()

//Soal 6 Check

console.log("Soal No. 6 (Metode Array)")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input)
