//Oleh: Dwiani Yulia A

//////////////
/// Soal 1 ///
//////////////

console.log("1. Mengubah fungsi menjadi fungsi arrow")
// ES5
/* const golden = function goldenFunction(){
 *    console.log("this is golden!!")
 *   }
 * 
 * golden()
 */

 // ES6
const golden = () => {
    return ("this is golden!!")
}
console.log(golden())
console.log()


//////////////
/// Soal 2 ///
//////////////

console.log("2. Sederhanakan menjadi Object literal di ES6")

// ES5
/* const newFunction = function literal(firstName, lastName){
 *   return {
 *     firstName: firstName,
 *     lastName: lastName,
 *     fullName: function(){
 *       console.log(firstName + " " + lastName)
 *       return 
 *     }
 *   }
 * }
 */

 // ES6
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
}
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  console.log()


//////////////
/// Soal 3 ///
//////////////

console.log("3. Destructuring")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// ES5
/* const firstName = newObject.firstName;
 * const lastName = newObject.lastName;
 * const destination = newObject.destination;
 * const occupation = newObject.occupation;
 */

 // ES6
 const {firstName, lastName, destination, occupation, spell} = newObject;

 // Driver code
console.log(firstName, lastName, destination, occupation)
console.log()

//////////////
/// Soal 4 ///
//////////////

console.log("4. Array Spreading")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// ES5
// const combined = west.concat(east)

// ES6
const combined = [...west,...east]

//Driver Code
console.log(combined)
console.log()

//////////////
/// Soal 5 ///
//////////////

console.log("5. Template Literals")

const planet = "earth"
const view = "glass"

// ES5
/* var before = 'Lorem ' + view + 'dolor sit amet, ' +  
 *    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
 *    'incididunt ut labore et dolore magna aliqua. Ut enim' +
 *    ' ad minim veniam'
 */

 // ES6
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 