//Oleh: Dwiani Yulia Ariyanti


//////////////
/// Soal 1 ///
//////////////

console.log('No.1 Looping While')
// LOOPING PERTAMA
console.log('LOOPING PERTAMA')
var count = 0
while (count <= 20){
    console.log(count + ' - I love coding')
    count += 2
}

//LOOPING KEDUA
console.log('LOOPING KEDUA')
while (count > 0){
    count -= 2
    console.log(count + ' - I will become a mobile developer')
}
console.log()


//////////////
/// Soal 2 ///
//////////////

console.log('No.2 Looping menggunakan for')
for (var i = 1; i <= 20; i++){
    if (i % 2 == 1){
        if (i % 3 == 0){
            console.log(i + ' - I Love Coding ')
        } else {
            console.log(i + ' - Santai ')
        }
    } else {
        console.log(i + ' - Berkualitas ')
    }
}
console.log()


//////////////
/// Soal 3 ///
//////////////

console.log('No. 3 Membuat Persegi Panjang')
for (var row = 1; row <= 4; row++){
    console.log('########')
}
console.log()


//////////////
/// Soal 4 ///
//////////////

console.log('No. 4 Membuat Tangga')
var str = '#'
var str2 = ''
for (row = 1; row <= 7; row++){
    str2 += str
    console.log(str2)
}
console.log()


//////////////
/// Soal 5 ///
//////////////

console.log('No. 5 Membuat Papan Catur')
for ( i = 1; i <= 8; i++){
    if (i % 2 == 1){
        console.log(' # # # #')
    } else {
        console.log('# # # # ')
    }
}