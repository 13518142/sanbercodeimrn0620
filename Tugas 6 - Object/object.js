//Oleh: Dwiani Yulia A

//////////////
/// Soal 1 ///
//////////////


function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    for (var i = 0; i < arr.length; i++){
        var first_info = arr[i]
        var object = {
            firstName    : first_info[0] ,
            lastName     : first_info[1] ,
            gender       : first_info[2],
            age          : (first_info[3] < thisYear) ? (thisYear - first_info[3]).toString() : "Invalid birth year"
        }
        console.log(i+1 + ". " + object.firstName + " " + object.lastName + ":")
        console.log(object)    
    }
}


//////////////
/// Soal 2 ///
//////////////

function shoppingTime(member_id, input_money) {
    list_barang = [["Sepatu Stacattu", 1500000],["Baju Zoro", 500000], ["Baju H&N", 250000],["Sweater Uniklooh", 175000],["Casing Handphone", 50000]]
    list_affordable = []
    cost = 0
    for (var i = 0; i < list_barang.length; i++){
        if (input_money > list_barang[i][1]){
            list_affordable.push(list_barang[i][0])
            cost += list_barang[i][1]
        }
    }
    if (member_id == '' || member_id == null){
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
        return ''
    } else {
        if (input_money < 50000){
            console.log("Mohon maaf, uang tidak cukup")
            return ''
        } else {
            var object = {
                memberId        : member_id,
                money           : input_money,
                listPurchased   : list_affordable,
                changeMoney     : input_money - cost
            }
            return object        
        }
    }
}

//////////////
/// Soal 3 ///
//////////////

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    arr = []

    for (var i = 0; i < arrPenumpang.length; i++){
        info = arrPenumpang[i]
        // for (var j = 0; j < rute.length; j++){
        //     if (info[1] == rute[j]){
        //         var multiplier = 0
        //         // while (info[2] != rute[j]){
        //         //     multiplier++
        //         // }
        //     }
        // }
        var object = {
            penumpang   : info[0],
            naikDari    : info[1],
            tujuan      : info[2],
            bayar       : 2000 * (rute.indexOf(info[2]) - rute.indexOf(info[1]))
        }
        arr.push(object)
    }
    return arr
}


/// TEST ///

// Driver Code - No. 1 (Array to Object)
console.log("No. 1 (Array to Object)")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
console.log() 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
console.log()
arrayToObject([]) // ""

// TEST CASES - No. 2 (Shopping Time)
console.log("No. 2 (Shopping Time)")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log()

//TEST CASE - No. 3 (Naik Angkot)
console.log("No. 3 (Naik Angkot)")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]