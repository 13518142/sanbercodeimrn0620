//Oleh: Dwiani Yulia Ariyanti

//////////////
/// Soal 1 ///
//////////////

function teriak(){
    return("Halo Sanbers!")
}

//Soal 1 - Check
console.log("No. 1")
console.log(teriak())
console.log()


//////////////
/// Soal 2 ///
//////////////

function kalikan(a, b){
    return(a*b)
}

//Soal 2 - Check
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log("No. 2")
console.log(hasilKali)
console.log()



//////////////
/// Soal 3 ///
//////////////

function introduce(name, age, address, hobby){
    return("Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby +"!")
}

//Soal 3 - Check
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming" 
var perkenalan = introduce(name, age, address, hobby)
console.log("No. 3")
console.log(perkenalan)
console.log()